﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;

namespace SecureFlight.Core.Services;

public class BaseService<TEntity> : IService<TEntity>
    where TEntity : class
{
    private readonly IRepository<TEntity> _repository;

    public BaseService(IRepository<TEntity> repository)
    {
        _repository = repository;
    }

    public async Task<OperationResult<TEntity>> GetByIdAsync(object id)
    {
        return new OperationResult<TEntity>(await _repository.GetByIdAsync(id));
    }

    public async Task<OperationResult<IReadOnlyList<TEntity>>> GetAllAsync()
    {
        return new OperationResult<IReadOnlyList<TEntity>>(await _repository.GetAllAsync());
    }

    public async Task<OperationResult<TEntity>> Add(TEntity entity)
    {
        return new OperationResult<TEntity>(await _repository.AddAsync(entity));
    }

    public async Task<OperationResult<TEntity>> AddPassengerToFlight(string passid, long fid)
    {
        //More work todo here
        throw new NotImplementedException();
    }
}

//public class PassengerFlightService : BaseService
//{ 
    
//}